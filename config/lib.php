<?php require_once("conn.php") ?>
<?php
// TODO: rewrite on a prepared query
/**
 * Select All Post
 *
 * @param string $pole
 * @param string $sort
 * @return array $posts
 */
function showAllPost($column = "created_at", $sort = "DESC", $firstPost = 0, $postPerPage)
{
    global $link;
    $querySelect = "SELECT `p`.`id`,`p`.`title`,`p`.`content`,`p`.`image`,`p`.`created_at`,`p`.`user_id`,`u`.`email`
                    FROM `post` `p`
                    INNER JOIN `user` `u` ON `p`.`user_id` = `u`.`id`
                    ORDER BY {$column} {$sort}
                    LIMIT {$firstPost}, {$postPerPage}";
    $result = mysqli_query($link, $querySelect);
    if (mysqli_affected_rows($link) > 0) {
        $posts = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row['excerpt'] = getExcerpt($row['content']);
            $posts[] = $row;
        }
        return $posts;
    } else
        return false;
}

/**
 * Select count recording
 *
 * @param string $table
 * return int $count | bool
 */
function countElem($table = 'post')
{
    global $link;
    $query = "SELECT COUNT( DISTINCT `id` ) FROM {$table}";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) > 0) {
        $count = array();
        while ($row = mysqli_fetch_array($result, MYSQL_NUM)) {
            $count[] = $row;
        }
        return $count[0][0];
    } else
        return false;
}
//
/**
 * Show All Users
 *
 * @return array | bool
 */
function showAllUser($firstUser = 0, $countUsers)
{
    global $link;
    $querySelect = "SELECT `u`.`id`, `u`.`email`, COUNT( p.user_id )
                    FROM user `u`
                    LEFT JOIN `post` `p` ON `u`.`id` = `p`.`user_id`
                    GROUP BY `u`.`email`, `p`.`user_id`
                    ORDER BY `u`.`id`
                    LIMIT {$firstUser}, {$countUsers}";
    $result = mysqli_query($link, $querySelect);
    if (mysqli_affected_rows($link) > 0) {
        $users = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $users[] = $row;
        }
        return $users;
    } else {
        return false;
    }
}

//
/**
 * Delete User
 *
 * @param int $id_user
 * @return bool
 */
function deleteUser($idUser)
{
    global $link;
    $querySelect = "SELECT `user_id` FROM `post` WHERE `user_id` = {$idUser}";
    mysqli_query($link, $querySelect);
    if (mysqli_affected_rows($link) > 0) {
        $queryDelete = "DELETE `post`, `user`
                    FROM `post`, `user`
                    WHERE `post`.`user_id` = `user`.`id` AND `user`.`id` = {$idUser}";
    } else {
        $queryDelete = "DELETE `user`
                        FROM  `user`
                        WHERE `id` = {$idUser}";
    }
    mysqli_query($link, $queryDelete);
    if (mysqli_affected_rows($link) > 0)
        return true;
    else
        return false;
}

//
/**
 * Get User
 *
 * @param int $user_id
 * @return bool | array users
 */
function getUser($user_id)
{
    global $link;
    $querySelect = "SELECT `id`, `email`, `avatara`
              FROM `user`
              WHERE `id` ='{$user_id}'";
    $result = mysqli_query($link, $querySelect);
    if (mysqli_affected_rows($link) > 0) {
        $users = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $users[] = $row;
        }
        return $users[0];
    } else
        return FALSE;
}

//

/**
 * Edit User
 *
 * @param int $user_id
 * @param string $email
 * @param srting(url) $image
 * @return bool
 */
function editUser($idUser, $email, $image)
{
    global $link;
    $queryUpdate = "UPDATE `user` SET `email`='{$email}',`avatara`='{$image}' WHERE `id`='{$idUser}'";
    mysqli_query($link, $queryUpdate);
    if (mysqli_affected_rows($link) > 0)
        return true;
    else
        return false;
}

//
//
/**
 * Get post
 *
 * @param $id_post
 * @return bool or array post
 */
function getPost($idPost)
{
    global $link;
    $querySelect = "SELECT `p`.`id`,`p`.`title`,`p`.`content`,`p`.`image`,`p`.`created_at`,`p`.`user_id`,`u`.`email`
                    FROM `post` `p`
                    INNER JOIN user `u` ON `p`.`user_id` = `u`.`id`
                    WHERE `p`.`id` ={$idPost}";
    $result = mysqli_query($link, $querySelect);
    if (mysqli_affected_rows($link) != -1) {
        if (mysqli_affected_rows($link) != 0) {
            $post = array();
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $post[] = $row;
            }
            return $post[0];
        } else {
            return null;
        }
    } else
        return false;
}

//
/**
 * Get a brief description of the post
 *
 * @param $str
 * @param int $len
 * @return string
 */
function getExcerpt($str, $len = 250)
{
//    $res = mb_substr($str, 0, $len);
//    $lastChar = $res[mb_strlen($res)-1];
//    while($lastChar != " " || $lastChar == ",") {
//        $res = mb_substr($res, 0, -1);
//        $lastChar = $res[mb_strlen($res)-1];
//    }
//    $res = mb_substr($res, 0, -1);
//    return $res;
    $res = mb_substr($str, 0, $len);
    $len = strrpos($res, ' ');
    $zpt = strrpos($res, ',');
    if ($zpt == ($len - 1)) {
        $len = $len - 1;
    }
    $res = mb_substr($str, 0, $len);
    return $res;
}

//
/**
 * Get comment for post
 *
 * @param int $id_post
 * @return array|bool
 */
function getComment($id_post)
{
    global $link;
    $querySelect = "SELECT `c`.`id`,`c`.`content`,`c`.`user_id`,`c`.`post_id`,`c`.`created_comment`,`u`.`email`,`u`.`avatara`
              FROM `comment` `c`
              INNER JOIN `post` `p` ON `c`.`post_id` = `p`.`id`
			  INNER JOIN `user` `u` ON `c`.`user_id` = `u`.`id`
			  WHERE `p`.`id`={$id_post} AND `c`.`post_id`={$id_post}";
    $resultComments = mysqli_query($link, $querySelect);
    $arrayComments = array();
    if (mysqli_affected_rows($link) != -1) {
        while ($row = mysqli_fetch_array($resultComments, MYSQLI_ASSOC)) {
            $arrayComments[] = $row;
        }
        return $arrayComments;
    } else {
        return FALSE;
    }
}

//
//Add comment
/**
 * @param string valid email $email
 * @param string $text
 * @param int $id_post
 * @return bool
 */
function addComment($email, $text, $id_post)
{
    global $link;
    $querySelect = "SELECT `email` FROM `user` WHERE `email`='{$email}'";
    $result = mysqli_query($link, $querySelect);
    $comments = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $comments[] = $row;
    }
    if (empty($comments)) {
        //add user - email
        $queryInsert = "INSERT INTO `user`(`email`) VALUES ('{$email}')";
        mysqli_query($link, $queryInsert);
        //
        //Select data new user
        $queryUser = "SELECT `id`, `email` FROM `user` WHERE `email`='{$email}'";
        $resUser = mysqli_query($link, $queryUser);
        $arrUser = array();
        while ($rowUser = mysqli_fetch_array($resUser, MYSQLI_ASSOC)) {
            $arrUser[] = $rowUser;
        }
        //Insert new comment
        $addTo = time() + (3 * 60 * 60);
        $queryInsert = "INSERT INTO `comment`(`content` , `created_comment` , `user_id` , `post_id`) VALUES ('{$text}','{$addTo}','{$arrUser[0]['id']}','{$id_post}')";
        mysqli_query($link, $queryInsert);
        header("location: /index.php?q=article&id=$id_post");
    } else {
        $queryUser = "SELECT `id`, `email` FROM `user` WHERE `email`='{$email}'";
        $resUser = mysqli_query($link, $queryUser);
        $arrUser = array();
        while ($rowUser = mysqli_fetch_array($resUser, MYSQLI_ASSOC)) {
            $arrUser[] = $rowUser;
        }
        //
        //Insert new comment
        $addTo = time() + (3 * 60 * 60);
        $queryInsert = "INSERT INTO `comment`(`content` , `created_comment` , `user_id` , `post_id`) VALUES ('{$text}','{$addTo}','{$arrUser[0]['id']}','{$id_post}')";
        mysqli_query($link, $queryInsert);
    }
    if (mysqli_affected_rows($link) > 0)
        return true;
    else
        return false;
}

//
//
/**
 * Clear string
 *
 * @param string $str
 * @return string
 */
function clearStr($str)
{
    global $link;
    $str = trim(strip_tags(mysqli_real_escape_string($link, $str)));
    return $str;
}

//
/**
 * Clear int
 *
 * @param int $int
 * @return int
 */
function clearInt($int)
{
    $int = (int)$int;
    return $int;
}

//
//
/**
 * Add post
 *
 * @param string valid email $email
 * @param string $titlePost
 * @param string $fieldText
 * @param string url $image
 * @return bool
 */
function addPost($email, $titlePost, $fieldText, $image)
{
    global $link;
//
    $query = "SELECT `email` FROM `user` WHERE `email`='{$email}'";
    $res = mysqli_query($link, $query);
    $arr = array();
    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
        $arr[] = $row;
    }
    if (empty($arr)) {
        //add user - email
        $queryInsert = "INSERT INTO `user`(`email`) VALUES ('{$email}')";
        mysqli_query($link, $queryInsert);
        //
        //Select data new user
        $queryUser = "SELECT `id`, `email` FROM `user` WHERE `email`='{$email}'";
        $resUser = mysqli_query($link, $queryUser);
        $arrUser = array();
        while ($rowUser = mysqli_fetch_array($resUser, MYSQLI_ASSOC)) {
            $arrUser[] = $rowUser;
        }
        //Insert new post
        $addTo = time() + (3 * 60 * 60);
        $queryInsert = "INSERT INTO `post`(`title` , `content` , `image` , `created_at` , `user_id`)
                        VALUES ('{$titlePost}' , '{$fieldText}' , '{$image}' , '{$addTo}' , '{$arrUser[0]['id']}')";
        mysqli_query($link, $queryInsert);
    } else {
        //echo "User not new";
        $queryUser = "SELECT `id`, `email` FROM `user` WHERE `email`='{$email}'";
        $resUser = mysqli_query($link, $queryUser);
        $arrUser = array();
        while ($rowUser = mysqli_fetch_array($resUser, MYSQLI_ASSOC)) {
            $arrUser[] = $rowUser;
        }
        //
        //Insert new comment
        $addTo = time() + (3 * 60 * 60);
        $queryInsert = "INSERT INTO `post`(`title`, `content`, `image`, `created_at` , `user_id`)
                        VALUES ('{$titlePost}', '{$fieldText}', '{$image}', '{$addTo}' , '{$arrUser[0]['id']}')";
        mysqli_query($link, $queryInsert);
    }
    if (mysqli_affected_rows($link) > 0)
        return TRUE;
    else
        return FALSE;
}

//
/**
 * Edit post
 *
 * @param id $post_id
 * @param string valid email$email
 * @param $title_post
 * @param $field_text
 * @param $image
 * @return bool
 */
function editPost($postId, $email, $titlePost, $fieldText, $image)
{
    global $link;
    $queryUpdate = "UPDATE `post` SET `title`='{$titlePost}',`content`='{$fieldText}',`image`='{$image}' WHERE `id`={$postId}";
    mysqli_query($link, $queryUpdate);
    if (mysqli_affected_rows($link) > 0)
        return TRUE;
    else
        return FALSE;
}

//
/**
 * Delete post
 *
 * @param int $id
 * @return bool
 */
function deletePost($id)
{
    global $link;
    $queryDelete = "DELETE `post` FROM `post` WHERE `post`.`id` = {$id}";
    mysqli_query($link, $queryDelete);
    if (mysqli_affected_rows($link) > 0)
        return true;
    else
        return false;
}

//
/**
 * Delete comment
 *
 * @param int $id
 * @return bool
 */
function deleteComment($id)
{
    global $link;
    $queryDelete = "DELETE FROM `comment` WHERE `comment`.`id` = {$id};";
    mysqli_query($link, $queryDelete);
    if (mysqli_affected_rows($link) > 0)
        return TRUE;
    else
        return FALSE;
}

//
/**
 * Select tag
 *
 * @param int $idPost
 * @return array tags
 */
function selectTag($idPost)
{
    global $link;
    $querySelect = "SELECT `p`.`id`, `t`.`name` FROM `post` AS `p`
                    LEFT JOIN `post-tag` AS `pt` ON `p`.`id` = `pt`.`post_id`
                    LEFT JOIN `tag` AS `t` ON `pt`.`tag_id` = `t`.`id`
                    WHERE `p`.`id` = {$idPost}";
    $result = mysqli_query($link, $querySelect);
    $tags = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $tags[] = $row;
    }
    return $tags;
}

//
/**
 * Select Email
 *
 * @param $idPost
 * @return mixed
 */
function selectEmail($idPost)
{
    global $link;
    $querySelect = "SELECT `u`.`email`
                    FROM `post` `p`
                    INNER JOIN `user` `u` ON `p`.`user_id` = `u`.`id`
                    WHERE `p`.`id` = {$idPost}";
    $result = mysqli_query($link, $querySelect);
    $email = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $email[] = $row;
    }
    return $email[0];
}

//
?>