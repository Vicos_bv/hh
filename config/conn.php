<?php
try {
    $link = mysqli_connect(
        'localhost', /* Host */
        'tester', /* Username */
        '123456', /* Password */
        'hh_bd'); /* Database for query default */
    if (!$link) {
        throw new Exception("Error connect to database");
    }
    mysqli_query($link, "SET NAMES utf8");
} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getCode();
    echo $e->getFile();
    echo $e->getLine();
}
session_start();