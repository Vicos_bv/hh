<?php require_once("config/conn.php") ?>
<?php require_once("config/lib.php")?>
<!DOCTYPE html>
<html>
<head>
    <!--CSS-->
    <link type="text/css" href="../css/style.css" rel="stylesheet" />
    <!--Other-->
    <meta charset="utf-8" />
    <!-- My Script -->
<!--    <script type="text/javascript" src="js/script.js"></script>-->
    <title>Заголовок</title>
</head>
<body>
<div id="wrap">
<!-- Header -->
<div id="header">
    <div class="block">
        <div id="logo" class="cell1">
            <a href="/index.php">Logo</a>
        </div>
        <div id="title">HabraHabr</div>
    </div>
    <div id="top_menu">
        <div class="block">
            <div class="cell2">
                <ul id="ul_menu">
                    <li><a <?php if ($_GET['q'] == 'index') {
                            echo "class='active'";
                        } ?> href="/index.php">Posts</a></li>
                </ul>
            </div>
            <div class="cell1">
                <div class="soc_links">
                    <ul>
                        <li><a id="vk" href="#">vk</a></li>
                        <li><a id="fb" href="#">fb</a></li>
                        <li><a id="tw" href="#">tw</a></li>
                        <li><a id="rss" href="#">rss</a></li>
                        <li><a id="ig" href="#">ig</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header End -->