<?php require_once("config/conn.php")?>
<?php require_once("config/lib.php")?>
<?php
header("Refresh: 3; http://".$_SERVER["SERVER_NAME"]);
?>
<?php require_once("header.tpl.php") ?>
    <div id="content">
        <!-- Content -->
        <div class="block">
            <h3>Nothing found(</h3>
            <h4>You'll be directed to the homepage after 3 seconds.</h4>
            <div class="clear"></div>
        </div>
        <div class="hfooter"></div>
    </div>
    <!-- End Content -->
    </div><!--wrap-->
<?php require_once("footer.tpl.php") ?>