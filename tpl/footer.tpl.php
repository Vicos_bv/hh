<div id="footer">
    <div class="pagination">
        <?php
        if ($page != 1) {
            echo "<a href=/index.php?page=".($page - 1)."><</a>";
        }
        for ($i = 1; $i <= $pages; $i++) {
            if ($i == $page) {
                echo "<a href=/index.php?page=$i><b>".$i."</b></a>";
            } else {
                echo "<a href=/index.php?page=$i>".$i."</a>";
            }
        }
        if ($page != $pages) {
            echo "<a href=/index.php?page=".($page + 1).">></a>";
        }
        ?>
    </div>
    <div class="block">
        <div id="logo_footer" class="cell4">
            <a href="#">Logo</a>
        </div>
        <div id="soc_link_footer" class="cell4">
            <div class="soc_links">
                <ul>
                    <li><a id="vk" href="#">vk</a></li>
                    <li><a id="fb" href="#">fb</a></li>
                    <li><a id="tw" href="#">tw</a></li>
                    <li><a id="rss" href="#">rss</a></li>
                    <li><a id="ig" href="#">ig</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
