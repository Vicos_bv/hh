<?php require_once("config/conn.php") ?>
<?php require_once("config/lib.php") ?>
<?php
session_destroy();
/**
 * Pagination
 */
if (!isset($_GET['page'])) {
    $_GET['page'] = 1;
}
$countPost = countElem();
$postPerPage = 5;
$pages = intval(($countPost - 1) / $postPerPage) + 1;
$page = clearInt($_GET['page']);
if (empty($page) || $page < 0) {
    $page = 1;
}
if ($page > $pages) {
    $page = $pages;
}
$_GET['page'] = $page;
$firstPost = $page * $postPerPage - $postPerPage;
//
/**
 * Check index
 */
if (!isset($_GET['q'])) {
    $_GET['q'] = 'index';
    $idPage = clearStr($_GET['q']);
    if (!isset($idPost)) {
        $result = showAllPost($column = "created_at", $sort = "DESC", $firstPost, $postPerPage);
    } else {
        $result = showAllPost($column = "created_at", $sort = "DESC", $firstPost, $countPost);
    }
} else {
    $idPage = clearStr($_GET['q']);
    if(isset($_GET['id'])){
        $idPost = clearStr($_GET['id']);
    }
    if (!isset($idPost)) {
        $result = showAllPost($column = "created_at", $sort = "DESC", $firstPost, $postPerPage);
    } else {
        $result = showAllPost($column = "created_at", $sort = "DESC", $firstPost, $countPost);
    }
}
//
?>
<?php
switch ($idPage) {
    /**
     * To display the index page
     */
    case 'index':
        include_once("tpl/index.tpl.php");
        break;
    //
    case 'article':
        /**
         * To display the post page
         */
        if (isset($idPost) && '' != $idPage) {
            $post = getPost(clearStr($idPost));
            if ($post == null || $post == false) {
                header("location: http://" . $_SERVER["SERVER_NAME"] . "/index.php?q=404");
            }
            $comments = getComment($idPost);
            if (!isset($_SESSION['errorValidEmail']) || $_SESSION['errorValidEmail'] !== 1) {
                $_SESSION['errorValidEmail'] = '';
            }
            if (!isset($_SESSION['errorAddComment']) || $_SESSION['errorAddComment'] !== 1) {
                $_SESSION['errorAddComment'] = '';
            }
            $arrayDataComment[0] = '';
            $arrayDataComment[1] = '';
        }
        if (isset($_SESSION['dataComment'])) {
            $arrayDataComment = json_decode($_SESSION['dataComment']);
        }
        include_once("tpl/article.tpl.php");
        break;
    /**
     * To display the 404 page
     */
    case '404' :
        include_once("tpl/404.tpl.php");
    default :
        include_once("tpl/404.tpl.php");
    //
}
?>