<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
/**
 * Delete Post
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_GET['delete'])) {
        $deletePost = clearInt($_GET['delete']);
        if ($deletePost != 0) {
            $resultDelete = deletePost($deletePost);
            if ($resultDelete === true) {
                header("Location: /crud/index.php");
            } else {
                header("Location: /crud/index.php?error=errorDelete");
            }
        } else {
            header("Location: /crud/index.php?error=errorDelete");
        }
    } else {
        header("Location: /crud/index.php?error=errorDelete");
    }
} else {
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=index");
}

?>