<?php require_once("header.tpl.php") ?>
    <div id="content">
        <div class="block">
            <!-- Content -->
            <div class="post">
                <?php if (isset($_SESSION['successAdd'])) {
                    echo $_SESSION['successAdd'];
                } ?>
                <div class="cell2">
                    <form id="admin_form" action="" method="post">
                        <!--foreach-->
                        <?php foreach ($resultPosts as $article) { ?>
                            <div class="elem_admin">
                                <label for="<?php echo $article['id'] ?>">
                                    <?php echo $article['title'] ?>
                                </label>
                                <span>(<?php echo $article['id'] ?>)</span>

                                <p class="post_info">
                                    <span><?php echo $article['email'] ?></span>
                                    <span><?php echo date('Y-m-d H:i:s', $article['created_at']) ?></span>
                                </p>

                                <div class="btn_admin">
                                    <a class="btn admin"
                                       href="<?php $_SERVER["SERVER_NAME"] ?>/crud/index.php?q=edit_post&edit=<?php echo $article['id'] ?>">Edit</a>
                                    <a class="btn admin"
                                       href="<?php $_SERVER["SERVER_NAME"] ?>/crud/delete.php?delete=<?php echo $article['id'] ?>">Delete</a>
                                </div>
                                <hr>
                            </div>
                            <!--endforeach-->
                        <?php } ?>
                </div>
                <div class="cell1">
                    <a class="btn admin" href="<?php $_SERVER["SERVER_NAME"] ?>/crud/index.php?q=add">Add</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="hfooter"></div>
    </div>
    <!-- End Content -->
    </div>
    <!--wrap-->
<?php require_once("footer.tpl.php") ?>