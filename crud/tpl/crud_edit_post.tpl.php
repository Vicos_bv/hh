<?php require_once("header.tpl.php") ?>
<div class="content">
    <div class="block">
        <div class="block">
            <?php if (isset($_SESSION['successEdit'])) {
                echo $_SESSION['successEdit'];
            } ?>
            <div class="cell2">
                <div class="admin_form_container">
                    <form id="admin_form" action="crud_edit.php" method="post">
                        <div id="wrap_field_id">
                            <?php
                            if (isset($_SESSION['validId'])) {
                                echo $_SESSION['validId'];
                            } ?>
                            <label for="field_id">Identification number of the post</label>
                            <input pattern="number" name="id" id="field_id" type="text" value="<?php echo $result['id'] ?>"
                                   disabled>
                            <input type="hidden" name="post_id" value="<?php echo $result['id'] ?>"/>
                        </div>

                        <div id="wrap_field_email">
                            <?php
                            if (isset($_SESSION['validEmail'])) {
                                echo $_SESSION['validEmail'];
                            } ?>
                            <label for="field_email">Email Address Author*</label>
                            <input name="email" id="field_email" required="required" type="email"
                                   value="<?php echo $result['email'] ?>" disabled
                                   pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$">
                            <input type="hidden" name="email" value="<?php echo $result['email'] ?>"/>
                        </div>

                        <div id="wrap_field_created">
                            <label for="field_created">Post created</label>
                            <input name="created_post" id="field_created" required="required" type="text"
                                   value="<?php echo date('Y-m-d H:i:s', $result['created_at']) ?>" disabled>
                        </div>

                        <div id="wrap_field_title">
                            <?php
                            if (isset($_SESSION['emptyTitle'])) {
                                echo $_SESSION['emptyTitle'];
                            } ?>
                            <label for="field_title">Title*</label>
                            <input name="title_post" id="field_title" required="required" type="text"
                                   value="<?php echo $result['title'] ?>" pattern="^[а-яА-ЯёЁa-zA-Z0-9\s\.]+$">
                        </div>
                        <?php
                        if (isset($_SESSION['emptyPost'])) {
                            echo $_SESSION['emptyPost'];
                        } ?>
                        <div id="wrap_field_text">
                            <label for="field_text">Text Area*</label>
                            <textarea rows="5" cols="20" name="field_text" id="field_text"
                                      required="required"><?php echo $result['content'] ?></textarea>
                        </div>
                        <div id="wrap_field_image">
                            <?php if (isset($_SESSION['validUrl'])) {
                                echo $_SESSION['validUrl'];
                            } ?>
                            <label for="field_image">Image</label>
                            <input name="image" id="field_image" required="required" type="url"
                                   value="<?php echo $result['image'] ?>">
                        </div>
                </div>
            </div>
            <div class="cell1">
                <div id="form_submit">
                    <input class="btn" value="Submit" type="Submit">
                </div>
                <div>
                    <a class="btn" href="/crud/index.php?q=index">Close</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <div class="hfooter"></div>
</div>
<!--wrap-->
<?php require_once("footer.tpl.php") ?>