<?php require_once("header.tpl.php") ?>
    <div id="content">
        <!-- Content -->
        <div class="block">
            <?php if (isset($_SESSION['successUpdateUser'])) {
                echo $_SESSION['successUpdateUser'];
            } ?>
            <?php if (isset($_SESSION['statusDeleteUser'])) {
                echo $_SESSION['statusDeleteUser'];
            } ?>
            <div class="post">
                <div class="cell2">
                    <form id="admin_form" action="edit_user.php" method="post">
                        <!--foreach-->
                        <?php foreach ($resultUsers as $user) { ?>
                            <div class="elem_admin">
                                <label for="<?php echo $user['id'] ?>">
                                    <?php echo $user['email'] ?>
                                </label>

                                <p class="post_info">
                                    <span>ID user: </span><span><?php echo $user['id'] ?></span>
                                    <span>Количество записей: </span><span><?php echo $user['COUNT( p.user_id )'] ?></span>
                                </p>

                                <div class="btn_admin">
                                    <a class="btn admin"
                                       href="<?php $_SERVER["SERVER_NAME"] ?>/crud/index.php?q=edit_user&edit=<?php echo $user['id'] ?>">Edit</a>
                                    <a class="btn admin"
                                       href="<?php $_SERVER["SERVER_NAME"] ?>/crud/delete_user.php?delete=<?php echo $user['id'] ?>">Delete</a>
                                </div>
                                <hr>
                            </div>
                            <!--endforeach-->
                        <?php } ?>
                </div>
                <div class="cell1">
                    </form>
                </div>
            </div>
        </div>
        <div class="hfooter"></div>
    </div>
    <!-- End Content -->
    </div>
    <!--wrap-->
<?php require_once("footer.tpl.php") ?>