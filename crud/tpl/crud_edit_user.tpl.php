<?php require_once("header.tpl.php") ?>
    <div>
        <!-- Content -->
        <div class="block">
            <div class="block">
                <?php if (isset($_SESSION['errorUpdateUser'])) {
                    echo $_SESSION['errorUpdateUser'];
                } ?>
                <div class="cell2">
                    <div class="admin_form_container">
                        <form id="admin_form" action="crud_edit_user.php" method="post">
                            <div id="wrap_field_id">
                                <label for="field_id">
                                    Identification number of the user
                                </label>
                                <input pattern="number" name="id" id="field_id" type="text"
                                       value="<?php echo $result['id'] ?>" disabled>
                                <input type="hidden" name="user_id" value="<?php echo $result['id'] ?>"/>
                            </div>

                            <div id="wrap_field_email">
                                <?php if (isset($_SESSION['errorValidEmail'])) {
                                    echo $_SESSION['errorValidEmail'];
                                } ?>
                                <label for="field_email">
                                    Email Address User*
                                </label>
                                <input name="email" id="field_email" required="required" type="email"
                                       value="<?php echo $result['email'] ?>">
                            </div>

                            <div id="wrap_field_image_user">
                                <?php if (isset($_SESSION['errorValidUrl'])) {
                                    echo $_SESSION['errorValidUrl'];
                                } ?>
                                <label for="field_image">
                                    Image
                                </label>
                                <input name="image" id="field_image" required="required" type="url"
                                       value="<?php echo $result['avatara'] ?>">
                            </div>
                    </div>
                </div>
                <div class="cell1">
                    <div id="form_submit">
                        <input class="btn" name="update" value="Submit" type="Submit">
                    </div>
                    <div>
                        <a class="btn close" href="/crud/index.php?q=index">Close</a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="hfooter"></div>
</div>

    <!--wrap-->
<?php require_once("footer.tpl.php") ?>