<?php require_once("header.tpl.php") ?>
    <div id="content">
        <!-- Content -->
        <div class="block">
            <div class="block">
                <div class="cell2">

                    <div class="admin_form_container">
                        <form id="admin_form" action="crud_add.php" method="post">
                            <div id="wrap_field_email">
                                <label for="field_email">
                                    Email Address Author*
                                </label>
                                <input name="email" id="field_email" required="required" type="email" value="<?php echo $result['email'] ?>"
                                       pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$">
                            </div>
                            <?php if (isset($_SESSION['emptyTitle'])) {
                                echo $_SESSION['emptyTitle'];
                            } ?>
                            <div id="wrap_field_title">
                                <label for="field_title">
                                    Title
                                </label>
                                <input name="title_post" id="field_title" required="required" type="text" value="<?php echo $result['title'] ?>"
                                       pattern="^[а-яА-ЯёЁa-zA-Z0-9\s\.]+$">
                            </div>
                            <div id="wrap_field_text">
                                <?php if (isset($_SESSION['emptyPost'])) {
                                    echo $_SESSION['emptyPost'];
                                } ?>
                                <label for="field_text">
                                    Text Area*
                                </label>
                                <textarea rows="5" cols="20" name="field_text" id="field_text"
                                          required="required"><?php echo $result['content'] ?></textarea>
                            </div>
                            <div id="wrap_field_image">
                                <?php if (isset($_SESSION['validUrl'])) {
                                    echo $_SESSION['validUrl'];
                                } ?>
                                <label for="field_image">
                                    Image
                                </label>
                                <input name="image" id="field_image" required="required" type="url" value="<?php echo $result['image'] ?>">
                            </div>
                    </div>
                </div>
                <div class="cell1">
                    <div id="form_submit">
                        <input class="btn" value="Submit" type="Submit">
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="hfooter"></div>
    </div>
    <!-- End Content -->
    <div class="hfooter"></div>
    </div>
    <!--wrap-->
<?php require_once("footer.tpl.php") ?>