<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $postId = clearInt($_POST['post_id']);
    $validEmail = selectEmail($postId);
    $email = clearStr($_POST['email']);
    $titlePost = clearStr($_POST['title_post']);
    $postText = clearStr($_POST['field_text']);
    $image = clearStr($_POST['image']);

    $arrayDataPost = array('email' => $email,
        'titlePost' => $titlePost,
        'fieldText' => $postText,
        'image' => $image);
    $arrayDataPost = json_encode($arrayDataPost);
    /**
     * Validation Email
     */
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['validEmail'] = '<div class="error_block"><p>Invalid email addresses</p></div>';
    }
    //
    /**
     * Checking emptiness
     */
    if (empty($titlePost)) {
        $_SESSION['emptyTitle'] = '<div class="error_block"><p>Must not be empty</p></div>';
    }
    if (empty($postText)) {
        $_SESSION['emptyPost'] = '<div class="error_block"><p>Must not be empty</p></div>';
    }
    //
    /**
     * Validation url
     */
    if (!filter_var($image, FILTER_VALIDATE_URL)) {
        $_SESSION['validUrl'] = '<div class="error_block"><p>Invalid url addresses</p></div>';
    }
    //
    if (!empty($email) && !empty($titlePost) && !empty($postText) && !empty($image)) {
        if ($validEmail['email'] == $_POST['email'] && filter_var($_POST['image'], FILTER_VALIDATE_URL)) {
            editPost($postId, $email, $titlePost, $postText, $image);
            $_SESSION['successEdit'] = '<div class="success_block"><p>Your post edited successfully</p></div>';
            header("location: /crud/index.php?q=edit_post&edit=$postId");
        } else {
            $_SESSION['dataPost'] = $arrayDataPost;
            header("location: /crud/index.php?q=edit_post&edit=$postId");
        }
    } else {
        $_SESSION['dataPost'] = $arrayDataPost;
        header("location: /crud/index.php?q=edit_post&edit=$postId");
    }
} else {
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=index");
}
?>