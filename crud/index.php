<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
session_destroy();
/**
 * Pagination
 */
if (!isset($_GET['page'])) {
    $_GET['page'] = 1;
}
//
if (!isset($_GET['q'])) {
    $_GET['q'] = 'index';
    $idPage = clearStr($_GET['q']);
} else {
    $idPage = clearStr($_GET['q']);
}
?>
<?php
switch ($idPage) {
    /**
     * To display the index page
     */
    case 'index' :
        /**
         *
         */
        $countPost = countElem();
        $postPerPage = 5;
        $pages = intval(($countPost - 1) / $postPerPage) + 1;
        $page = clearInt($_GET['page']);
        if (empty($page) || $page < 0) {
            $page = 1;
        }
        if ($page > $pages) {
            $page = $pages;
        }
        $_GET['page'] = $page;
        $firstPost = $page * $postPerPage - $postPerPage;
        //
        $resultPosts = showAllPost($column = "id", $sort = "DESC", $firstPost, $postPerPage); //Sort by the value id
        include_once("tpl/crud_index.tpl.php");
        break;
    /**
     * To display the users page
     */
    case 'users' :
        /**
         *
         */
        $countUser = countElem("user");
        $userPerPage = 5;
        $pages = intval(($countUser - 1) / $userPerPage) + 1;
        $page = clearInt($_GET['page']);
        if (empty($page) || $page < 0) {
            $page = 1;
        }
        if ($page > $pages) {
            $page = $pages;
        }
        $_GET['page'] = $page;
        $firstUser = $page * $userPerPage - $userPerPage;
        //
        $resultUsers = showAllUser($firstUser, $userPerPage);
        include_once("tpl/crud_users.tpl.php");
        break;
    /**
     * To display the edit post page
     */
    case 'edit_post' :
        $postId = clearInt($_GET['edit']);
        if(isset($_SESSION['dataPost'])){
            $result = getPost($_GET['edit']);
            $arrayDataPost = json_decode($_SESSION['dataPost']);
            $result['id'] = $postId;
            $result['email'] = clearStr($arrayDataPost->email);
            $result['title'] = clearStr($arrayDataPost->titlePost);
            $result['content'] = clearStr($arrayDataPost->fieldText);
            $result['image'] = clearStr($arrayDataPost->image);
        } else {
            $result = getPost($_GET['edit']);
        }
        if (!isset($result)) {
            header("Location: /crud/index.php?error=nullId");
        }
        include_once("tpl/crud_edit_post.tpl.php");
        break;
    /**
     * To display the add post page
     */
    case 'add' ;
        if (isset($_SESSION['dataPost'])) {
            $arrayDataPost = json_decode($_SESSION['dataPost']);
            $result['email'] = clearStr($arrayDataPost->email);
            $result['title'] = clearStr($arrayDataPost->titlePost);
            $result['content'] = clearStr($arrayDataPost->fieldText);
            $result['image'] = clearStr($arrayDataPost->image);
        } else {
            $result['email'] = '';
            $result['title'] = '';
            $result['content'] = '';
            $result['image'] = '';
        }
        include_once("tpl/crud_add_post.tpl.php");
        break;
//    TODO: Редактирование юзера
    /**
     * To display the edit user page
     */
    case 'edit_user' :
        if (isset($_GET['edit'])) {
            $idUser = clearInt($_GET['edit']);
            if ($idUser == 0) {
                header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
            }
            if ($idUser != false) {
                $result = getUser($idUser);
            } else {
                header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
            }
        } else {
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
        }
        include_once("tpl/crud_edit_user.tpl.php");
        break;
    /**
     * To display the 404 page
     */
    case '404' :
        include_once("tpl/crud_404.tpl.php");
        break;
    /**
     * To display the 404 page
     */
    default :
        include_once("tpl/crud_404.tpl.php");
}
?>