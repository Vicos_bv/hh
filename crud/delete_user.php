<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
/**
 * Delete User
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['delete'])) {
        $deleteUser = clearInt($_GET['delete']);
        if ($deleteUser != 0) {
            $resultDelete = deleteUser($deleteUser);
            if ($resultDelete === true) {
                $_SESSION['statusDeleteUser'] = '<div class="success_block"><p>User successfully deleted</p></div>';
                header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
            } else {
                $_SESSION['statusDeleteUser'] = '<div class="success_block"><p>Error deleting user</p></div>';
                header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
            }
        } else {
            $_SESSION['statusDeleteUser'] = '<div class="success_block"><p>Error deleting user</p></div>';
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
        }
    } else {
        $_SESSION['statusDeleteUser'] = '<div class="success_block"><p>Error deleting user</p></div>';
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
    }
} else {
    $_SESSION['statusDeleteUser'] = '<div class="success_block"><p>Error deleting user</p></div>';
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
}
?>